using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarRental;

namespace CarRental_Test
{
    [TestClass]
    public class Vehicle_Tests
    {
        [TestMethod]
        public void getRented_IsRentedTrue()
        {
            Vehicle vehicle = new Vehicle("ZH 394 993", "Seat Alhambra", 50, VehicleType.Car);

            vehicle.getRented();
            Assert.IsTrue(vehicle.IsRented);
        }

        [TestMethod]
        public void getReturned_IsRentedFalse()
        {
            Vehicle vehicle = new Vehicle("ZH 394 993", "Seat Alhambra", 50, VehicleType.Car);

            vehicle.getReturned();
            Assert.IsFalse(vehicle.IsRented);
        }


    }
}
