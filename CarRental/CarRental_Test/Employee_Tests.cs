﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarRental;
using Microsoft.OData.Edm;

namespace CarRental_Test
{
    [TestClass]
    public class Employee_Tests
    {
        [TestMethod]
        public void calculateEndPrice_positivNumbers_positivResult()
        {
            Employee employee = new Employee("Doe", "Joe", Date.Parse("1980-01-01"), "Advisor");

            double price = employee.calculateEndPrice(5, 20);

            Assert.AreEqual(100,price);
        }

        [TestMethod]
        public void calculateEndPrice_negativeNumbers_exception()
        {
            Employee employee = new Employee("Doe", "Joe", Date.Parse("1980-01-01"), "Advisor");

            double price = employee.calculateEndPrice(-5, 20);

            Assert.AreEqual(-1, price);
        }
    }
}
