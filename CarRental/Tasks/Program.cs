﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace Exercises
{
    class Program
    {
        static string fileName = "clientfile.bin";
        static void Main(string[] args)
        {
            //byteArrayInFile byteArrayInFile = new byteArrayInFile();
            //byteArrayInFile.byteArrayIntoFile();

            //writeInTxt writeInTxt = new writeInTxt();
            //writeInTxt.writeIntoTxt();

            
           
                Client[] clients = new Client[2];
                clients[0] = new Client(1, "Joe", "Hanson", 1.234, 6300);
                clients[1] = new Client(2, "Tina", "Turner", 4.5, 8300);
                Console.WriteLine("Safed Client;\n");

                foreach (Client count in clients)
                {
                    count.printClient();
                    Console.WriteLine();
                }
                FileStream fs = new FileStream(fileName, FileMode.Create);
                IFormatter bf = new BinaryFormatter();

                // Writes an array of objects to a binary file
                bf.Serialize(fs, clients);
                fs.Position = 0;
                Console.WriteLine("\n\nReconstructed Clients:\n");

                // Reads an array of objects from a binary file
                Client[] recClients = (Client[])bf.Deserialize(fs);

                foreach (Client count in recClients)
                {
                    count.printClient();
                    Console.WriteLine();
                }
                fs.Close();
            
        }
    }
}
