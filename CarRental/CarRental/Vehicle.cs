﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRental
{
    [Serializable]
    public enum VehicleType
    {
        Car = 1,
        Truck = 2,
        Motorcycle = 3
    }
    public class Vehicle
    {
        public string NumberPlate { get; set; }
        public bool IsRented { get; set; }
        public string Name { get; set; }
        public double PricePerDay { get; set; }
        public VehicleType Type { get; set; }
        

        public Vehicle(string numberPlate, string name, double pricePerDay, VehicleType type)
        {
            NumberPlate = numberPlate;
            IsRented = false;
            Name = name;
            PricePerDay = pricePerDay;
            Type = type;
        }


        public void printInfos(int vehicleNumber)
        {
            Console.WriteLine($"{vehicleNumber}: {Name} - {PricePerDay} CHF per day");

            if (IsRented)
            {
                Console.WriteLine("(This vehicle is rented.)\n");
            }
            else
            {
                Console.WriteLine("\n");
            }
        }

        public void printAllTypes()
        {
            //Print all Vehicle types
            foreach (VehicleType enumValue in (VehicleType[])Enum.GetValues((typeof(VehicleType))))
            {
                Console.WriteLine($"{enumValue.GetHashCode()} {enumValue}");
            }
        }

        public void getRented()
        {
            IsRented = true;
        }

        public void getReturned()
        {
            IsRented = false;
        }
    }
}
