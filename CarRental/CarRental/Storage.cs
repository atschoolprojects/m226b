﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using CarRental.Interface;

namespace CarRental
{
    class Storage : IDataSerialize
    {
        public void Serialize(object data, string filePath)
        {

            JsonSerializer jsonSerializer = new JsonSerializer();
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }


            using (StreamWriter sw = new StreamWriter(filePath))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                jsonSerializer.Serialize(writer, data);
            }
        }

        public object Deserialize<T>(string filePath)
        {
            
            return JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(filePath));
        }
    }
}
