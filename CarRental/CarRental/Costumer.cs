﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRental
{
    class Costumer : Person
    {
        public int CostumerNumber { get; set; }

        public Costumer(int costumerNumber, string lastname, string firstname, DateTime birthday) :base(lastname, firstname, birthday)
        {
            CostumerNumber = costumerNumber;
        }

        public override void talk(string text, string speaker)
        {
            base.talk(text, speaker);
        }

        public void enterStore(List<Employee> employees)
        {
            employees[0].talk("A Client has entered the store.", employees[0].Job);

        }
        
        public void leaveStore(List<Employee> employees)
        {
            employees[0].talk("A Client has left the store.", employees[0].Job);

        }
    }
}
