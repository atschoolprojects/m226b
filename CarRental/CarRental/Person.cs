﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRental
{
    public abstract class Person
    {
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public DateTime Birthday { get; set; }

        public Person(string lastname, string firstname, DateTime birthday)
        {
            Lastname = lastname;
            Firstname = firstname;
            Birthday = birthday;
        }

        public virtual void talk(string text, string speaker)
        {
            //set fontcolor to white
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{text}");
            wait(200);
        }

        public virtual void wait(int milisecounds)
        {
            System.Threading.Thread.Sleep(milisecounds);

        }
    }
}
